# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_read, diplomacy_eval, diplomacy_print, diplomacy_solve

# -----------
# TestDiplomacy
# -----------


class TestDiplomacy (TestCase):
    # ----
    # read
    # ----

    def test_read_1(self):
        s = "A Madrid Hold\n"
        line = diplomacy_read(s)
        self.assertEqual(line, ["A", "Madrid", "Hold"])

    def test_read_2(self):
        s = "\n"
        line = diplomacy_read(s)
        self.assertEqual(line, [])

    def test_read_3(self):
        s = "B Barcelona Move Madrid\n"
        line = diplomacy_read(s)
        self.assertEqual(line, ["B", "Barcelona", "Move", "Madrid"])


    # ----
    # eval
    # ----

    def test_eval_1(self):
        outcome = diplomacy_eval({"A": ["Madrid", "Hold"]})
        self.assertEqual(outcome, {"A": ["Madrid"]})

    def test_eval_2(self):
        outcome = diplomacy_eval({"A": ["Madrid", "Hold"], "B": ["Barcelona", "Move", "Madrid"], "C": ["London", "Support", "B"]})
        self.assertEqual(outcome, {"A": ["[dead]"], "B": ["Madrid"], "C": ["London"]})

    def test_eval_3(self):
        outcome = diplomacy_eval({"A": ["Madrid", "Hold"], "B": ["Barcelona", "Move", "Madrid"], "C": ["London", "Support", "B"], "D": ["Austin", "Move", "London"]})
        self.assertEqual(outcome, {"A": ["[dead]"], "B": ["[dead]"], "C": ["[dead]"], "D": ["[dead]"]})

    # -----
    # print
    # -----

    def test_print_1(self):
        w = StringIO()
        diplomacy_print(w, {"A": ["Madrid", "Hold"]}, {"A": ["Madrid"]})
        self.assertEqual(w.getvalue(), "A Madrid Hold \n\nA Madrid \n")

    def test_print_2(self):
        w = StringIO()
        diplomacy_print(w, {"A": ["Madrid", "Hold"], "B": ["Barcelona", "Move", "Madrid"], "C": ["London", "Support", "B"]}, {"A": ["[dead]"], "B": ["Madrid"], "C": ["London"]})
        self.assertEqual(w.getvalue(), "A Madrid Hold \nB Barcelona Move Madrid \nC London Support B \n\nA [dead] \nB Madrid \nC London \n")

    def test_print_3(self):
        w = StringIO()
        diplomacy_print(w, {"A": ["Madrid", "Hold"], "B": ["Barcelona", "Move", "Madrid"], "C": ["London", "Support", "B"], "D": ["Austin", "Move", "London"]}, {"A": ["[dead]"], "B": ["[dead]"], "C": ["[dead]"], "D": ["[dead]"]})
        self.assertEqual(w.getvalue(), "A Madrid Hold \nB Barcelona Move Madrid \nC London Support B \nD Austin Move London \n\nA [dead] \nB [dead] \nC [dead] \nD [dead] \n")

    # -----
    # solve
    # -----

    def test_solve_1(self):
        r = StringIO("A Madrid Hold\n\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Madrid Hold \n\nA Madrid \n")

    def test_solve_2(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\n\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Madrid Hold \nB Barcelona Move Madrid \nC London Support B \n\nA [dead] \nB Madrid \nC London \n")

    def test_solve_3(self):
        r = StringIO(
            "A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\nD Austin Move London\n\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Madrid Hold \nB Barcelona Move Madrid \nC London Support B \nD Austin Move London \n\nA [dead] \nB [dead] \nC [dead] \nD [dead] \n")

# ----
# main
# ----


if __name__ == "__main__":
    main()

""" #pragma: no cover """