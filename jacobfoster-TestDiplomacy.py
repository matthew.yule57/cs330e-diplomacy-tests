#!/usr/bin/env python3

# -------------------------------
# projects/diplomacy/TestDiplomacy.py
# Copyright (C) 
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.6/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_read, diplomacy_print, diplomacy_solve, Army

# -----------
# TestDiplomacy
# -----------


class TestDiplomacy (TestCase):
    # ----
    # read
    # ----

    def test_read_1(self):
        std_in = "A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\n"
        inp = diplomacy_read(std_in)
        self.assertEqual(inp,  [["A", "Madrid", "Hold"], ["B", "Barcelona", "Move", "Madrid"], ["C", "London", "Move", "Madrid"],["D", "Paris", "Support", "B"]])

    def test_read_2(self):
        std_in = "A Madrid Hold\nB Barcelona Hold\nC London Hold\n"
        inp = diplomacy_read(std_in)
        self.assertEqual(inp,  [["A", "Madrid", "Hold"], ["B", "Barcelona", "Hold"], ["C", "London", "Hold"]]) 

    def test_read_3(self):
        std_in = "A Madrid Hold\n"
        inp = diplomacy_read(std_in)
        self.assertEqual(inp,  [["A", "Madrid", "Hold"]])

    def test_read_4(self):
        std_in = "A Madrid Move Barcelona\nB Barcelona Move Madrid\nC London Move Paris\nD Paris Move London\n"
        inp = diplomacy_read(std_in)
        self.assertEqual(inp,  [["A", "Madrid", "Move", "Barcelona"], ["B", "Barcelona", "Move", "Madrid"], ["C", "London", "Move", "Paris"],["D", "Paris", "Move", "London"]])


    # ----
    # solve
    # ----

    def test_solve_1(self):
        r = StringIO("A Madrid Move Barcelona\nB Barcelona Move Madrid\nC London Move Paris\nD Paris Move London\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A Barcelona\nB Madrid\nC Paris\nD London\n")

    def test_solve_2(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\nD Paris Move London\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD [dead]\n")

    def test_solve_3(self):
        r = StringIO("A Madrid Hold\nB Barcelona Hold\nC London Hold\nD Paris Hold\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A Madrid\nB Barcelona\nC London\nD Paris\n")

    def test_solve_4(self):
        r = StringIO("A Madrid Support B\nB Temple Hold\nC Waco Move Temple\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A Madrid\nB Temple\nC [dead]\n")

    def test_solve_5(self):
        r = StringIO("A Madrid Support B\nB Barcelona Hold\nC London Move Barcelona\nD Paris Support B\nE Waco Support B\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A Madrid\nB Barcelona\nC [dead]\nD Paris\nE Waco\n")

    def test_solve_6(self):
        r = StringIO("A Madrid Support B\nB Barcelona Support A\nC London Support A\nD Paris Support A\nE Chicago Support C\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A Madrid\nB Barcelona\nC London\nD Paris\nE Chicago\n")

    def test_solve_7(self):
        r = StringIO("A Austin Support E\nB Birmingham Support D\nC Chicago Move Ellinger\nD Dallas Move Ellinger\nE Ellinger Hold\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A Austin\nB Birmingham\nC [dead]\nD [dead]\nE [dead]\n")

    def test_solve_8(self):
        r = StringIO("A Barcelona Move Madrid\nB Madrid Hold\nC Paris Move London\n D London Support B\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD [dead]\n")

    # -----
    # print
    # -----

    def test_print_1(self):
        w = StringIO()
        a = Army("A", "Madrid", "Hold")
        diplomacy_print(w, a.name, a.status)
        self.assertEqual(w.getvalue(), "A Madrid\n")

    def test_print_2(self):
        w = StringIO()
        b = Army("B", "Houston", "Move", "Madison")
        b.status = "Madison"
        diplomacy_print(w, b.name, b.status)
        self.assertEqual(w.getvalue(), "B Madison\n")

    def test_print_3(self):
        w = StringIO()
        c = Army("C", "Madison", "Support", "A")
        c.status = "[dead]"
        diplomacy_print(w, c.name, c.status)
        self.assertEqual(w.getvalue(), "C [dead]\n")

# ----
# main
# ----

if __name__ == "__main__":
    main()

""" #pragma: no cover
$ coverage run --branch TestCollatz.py >  TestCollatz.out 2>&1


$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


$ coverage report -m                   >> TestCollatz.out



$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Collatz.py          12      0      2      0   100%
TestCollatz.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""
