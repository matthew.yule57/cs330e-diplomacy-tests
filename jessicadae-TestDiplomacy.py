#!/usr/bin/env python3

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import Army, diplomacy_read, diplomacy_eval, diplomacy_print, diplomacy_solve, search_by_location, search_by_name

# -----------
# TestDiplomacy
# -----------


class TestDiplomacy (TestCase):

    # ----
    # read
    # ----

    def test_read_1(self):
        s = "A Madrid Hold\n"
        a, b, c, d = diplomacy_read(s)
        self.assertEqual(a,  "A")
        self.assertEqual(b, "Madrid")
        self.assertEqual(c, "Hold")
        self.assertEqual(d, None)

    def test_read_2(self):
        s = "B Austin Support A\n"
        a, b, c, d = diplomacy_read(s)
        self.assertEqual(a,  "B")
        self.assertEqual(b, "Austin")
        self.assertEqual(c, "Support")
        self.assertEqual(d, "A")

    def test_read_3(self):
        s = "F Georgetown Move Madrid\n"
        a, b, c, d = diplomacy_read(s)
        self.assertEqual(a,  "F")
        self.assertEqual(b, "Georgetown")
        self.assertEqual(c, "Move")
        self.assertEqual(d, "Madrid")

    def test_read_4(self):
        s = "D Austin Hold\n"
        a, b, c, d = diplomacy_read(s)
        self.assertEqual(a,  "D")
        self.assertEqual(b, "Austin")
        self.assertEqual(c, "Hold")
        self.assertEqual(d, None)

    # ----
    # army
    # ----

    def test_army_1(self):
        army = Army("A", "Georgetown", "Hold")
        self.assertEqual(army.name, "A")
        self.assertEqual(army.location, "Georgetown")
        self.assertEqual(army.action, "Hold")

    def test_army_2(self):
        army = Army("A", "Georgetown", "Hold", None)
        self.assertEqual(army.name, "A")
        self.assertEqual(army.location, "Georgetown")
        self.assertEqual(army.action, "Hold")

    def test_army_3(self):
        army = Army("A", "Georgetown", "Support", "Austin")
        self.assertEqual(army.name, "A")
        self.assertEqual(army.location, "Georgetown")
        self.assertEqual(army.action, "Support")


    def test_army_4(self):
        army = Army("A", "Georgetown", "Move", "Austin")
        self.assertEqual(army.name, "A")
        self.assertEqual(army.location, "Austin")
        self.assertEqual(army.action, "Move")

    # -----
    # search_by_name
    # -----

    def test_search_by_name_1(self):
        armies = [
            Army("A", "Georgetown", "Hold"),
            Army("B", "Austin", "Support", "A"),
            Army("C", "Laredo", "Move", "Georgetown"),
        ]
        army = search_by_name(armies, "A")
        self.assertTrue(isinstance(army, Army))
        self.assertEqual(army.name, "A")
        self.assertEqual(army.location, "Georgetown")
        self.assertEqual(army.action, "Hold")

    def test_search_by_name_2(self):
        armies = [
            Army("A", "Georgetown", "Hold"),
            Army("B", "Austin", "Support", "A"),
            Army("C", "Laredo", "Move", "Georgetown"),
        ]
        army = search_by_name(armies, "B")
        self.assertTrue(isinstance(army, Army))
        self.assertEqual(army.name, "B")
        self.assertEqual(army.location, "Austin")
        self.assertEqual(army.action, "Support")

    def test_search_by_name_3(self):
        armies = [
            Army("A", "Georgetown", "Hold"),
            Army("B", "Austin", "Support", "A"),
            Army("C", "Laredo", "Move", "Georgetown"),
        ]
        army = search_by_name(armies, "C")
        self.assertTrue(isinstance(army, Army))
        self.assertEqual(army.name, "C")
        self.assertEqual(army.location, "Georgetown")
        self.assertEqual(army.action, "Move")


    # -----
    # search_by_location
    # -----

    def test_search_by_location_1(self):
        armies = [
            Army("A", "Georgetown", "Hold"),
            Army("B", "Austin", "Support", "A"),
            Army("C", "Laredo", "Move", "Georgetown"),
        ]
        armies = search_by_location(armies, "Austin")
        for army in armies:
            self.assertTrue(isinstance(army, Army))
        self.assertEqual(armies[0].name, "B")
        self.assertEqual(armies[0].location, "Austin")
        self.assertEqual(armies[0].action, "Support")
    
    def test_search_by_location_2(self):
        armies = [
            Army("A", "Georgetown", "Hold"),
            Army("B", "Austin", "Support", "A"),
            Army("C", "Laredo", "Move", "Georgetown"),
        ]
        armies = search_by_location(armies, "Georgetown")
        for army in armies:
            self.assertTrue(isinstance(army, Army))
        self.assertEqual(armies[0].name, "A")
        self.assertEqual(armies[0].location, "Georgetown")
        self.assertEqual(armies[0].action, "Hold")
        self.assertEqual(armies[1].name, "C")
        self.assertEqual(armies[1].location, "Georgetown")
        self.assertEqual(armies[1].action, "Move")

    def test_support_power_1(self):
        armies = [
            Army("A", "Georgetown", "Support", "B"),
            Army("B", "Austin", "Hold",)
        ]
        for army in armies:
            army.commit_action(armies)
        self.assertEqual(armies[0].power(), 0)

    def test_support_power_2(self):
        armies = [
            Army("A", "Georgetown", "Support", "B"),
            Army("B", "Austin", "Hold",)
        ]
        for army in armies:
            army.commit_action(armies)
        self.assertEqual(armies[1].power(), 1)

    def test_support_power_3(self):
        armies = [
            Army("A", "Georgetown", "Support", "B"),
            Army("B", "Austin", "Hold",),
            Army("C", "Laredo", "Support", "B"),
        ]
        for army in armies:
            army.commit_action(armies)
        self.assertEqual(armies[1].power(), 2)

    def test_support_power_4(self):
        armies = [
            Army("A", "Madrid", "Hold"),
            Army("B", "Barcelona", "Move", 'Madrid'),
            Army("C", "London", "Support", "B"),
        ]
        for army in armies:
            army.commit_action(armies)
        self.assertAlmostEqual(armies[0].power(), 0)
        self.assertAlmostEqual(armies[1].power(), 1)
        self.assertAlmostEqual(armies[2].power(), 0)

    def test_support_power_5(self):
        armies = [
            Army("A", "Madrid", "Hold"),
            Army("B", "Barcelona", "Support", 'A'),
            Army("C", "London", "Support", "B"),
        ]
        for army in armies:
            army.commit_action(armies)
        self.assertAlmostEqual(armies[0].power(), 1)
        self.assertAlmostEqual(armies[1].power(), 1)
        self.assertAlmostEqual(armies[2].power(), 0)

    def test_support_power_6(self):
        armies = [
            Army("A", "Madrid", "Hold"),
            Army("B", "Barcelona", "Support", 'D'),
            Army("C", "London", "Support", "D"),
            Army("D", "Houston", "Move", "A"),
            Army("E", "Galveston", "Move", "Barcelona"),
        ]
        for army in armies:
            army.commit_action(armies)
        self.assertAlmostEqual(armies[0].power(), 0)
        self.assertAlmostEqual(armies[1].power(), 0)
        self.assertAlmostEqual(armies[2].power(), 0)
        self.assertAlmostEqual(armies[3].power(), 1)
        self.assertAlmostEqual(armies[4].power(), 0)

    # -----
    # print
    # -----

    def test_print(self):
        # w = StringIO()
        # collatz_print(w, 1, 10, 20)
        # self.assertEqual(w.getvalue(), "1 10 20\n")
        pass

    # -----
    # solve
    # -----

    def test_solve_1(self):
        r = StringIO("A Madrid Hold\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Madrid\n"
            )

    def test_solve_2(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB Madrid\nC London\n"
            )

    def test_solve_3(self):
        # Both cities are have equal supports so they are both dead
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\n"
            )

    def test_solve_4(self):
        # C attempted to support B but was attacked by D. 
        # This invalidates C's support and turns the field into a set of two one-on-one matches where everyone dies
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\nD Ausitin Move London\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD [dead]\n"
            )

    def test_solve_5(self):
        # All 3 A B C move to the city Madrid and they all have no support so they all die
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\n"
            )
    
    def test_solve_6(self):
        # All 3 A B C move to the city Madrid 
        # B has support from D
        # A and C don't have any support so B wins
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB Madrid\nC [dead]\nD Paris\n"
            )

    def test_solve_7(self):
        # All 3 A B C move to the city Madrid 
        # B has support from D
        # A has support from E
        # A B and C die because there isn't a single army with largest support
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\nE Austin Support A\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD Paris\nE Austin\n"
            )

    def test_solve_8(self):
        # https://piazza.com/class/kyhddv2bc7c40?cid=157_f6
        r = StringIO("A Austin Support B\nB Boston Support C\nC Charlotte Hold\nD Dallas Move Boston\nE Edmonton Move Charlotte\nF FortWorth Support E\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Austin\nB Boston\nC [dead]\nD [dead]\nE Charlotte\nF FortWorth\n"
            )

    def test_solve_9(self):
        # https://piazza.com/class/kyhddv2bc7c40?cid=157_f5
        r = StringIO("A Madrid Support B\nB Austin Hold\nC Houston Move Austin\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Madrid\nB Austin\nC [dead]\n"
            )

    def test_solve_10(self):
        # https://piazza.com/class/kyhddv2bc7c40?cid=157_f4
        r = StringIO("A Madrid Move Barcelona\nB Barcelona Move Madrid\nC London Move Austin\nD Austin Move London\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Barcelona\nB Madrid\nC Austin\nD London\n"
            )

    def test_solve_11(self):
        # https://piazza.com/class/kyhddv2bc7c40?cid=157_f3
        r = StringIO("A Austin Move Houston\nB Houston Move Austin\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Houston\nB Austin\n"
            )

    def test_solve_12(self):
        # https://piazza.com/class/kyhddv2bc7c40?cid=155
        r = StringIO("A Madrid Hold\nB Barcelona Support A\nC London Support B ")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Madrid\nB Barcelona\nC London\n"
            )

    def test_solve_13(self):
        # https://piazza.com/class/kyhddv2bc7c40?cid=156
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Barcelona\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC Barcelona\n"
            )
    
    def test_solve_14(self):
        # Attackers have equal power
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\n"
            )

    def test_solve_15(self):
        # Attackers have less power
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support A\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Madrid\nB [dead]\nC London\n"
            )

    def test_solve_16(self):
        # Attackers have more power
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB Madrid\nC London\n"
            )

    def test_solve_17(self):
        # Test for sorting the armies alphabetically in output
        r = StringIO("B Madrid Hold\nA Barcelona Move Madrid\nC London Support B\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB Madrid\nC London\n"
            )

# ----
# main
# ----


if __name__ == "__main__":
    main()

""" #pragma: no cover
$ coverage run --branch TestCollatz.py >  TestCollatz.out 2>&1


$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


$ coverage report -m                   >> TestCollatz.out



$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Collatz.py          12      0      2      0   100%
TestCollatz.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""
