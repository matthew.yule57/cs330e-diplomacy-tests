# -----------
# imports
# -----------
from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_read, diplomacy_solve

# -----------
# TestDiplomacy
# -----------
class TestDiplomacy(TestCase):
    # tests for diplomacy_read
    def test_diplomacy_read0(self):
        s = "A Istanbul Hold\nB Moscow Move Istanbul\nC London Move Istanbul\nD Paris Hold\nE Venice Support A"
        l = diplomacy_read(s)
        self.assertEqual(l,  ["A Istanbul Hold", "B Moscow Move Istanbul", "C London Move Istanbul", "D Paris Hold", "E Venice Support A"])
    def test_diplomacy_read1(self):
        s = "A Istanbul Hold"
        l = diplomacy_read(s)
        self.assertEqual(l, ["A Istanbul Hold"])
    def test_diplomacy_read2(self):
        s = "A Istanbul Hold\nB Moscow Move Istanbul\nC London Move Istanbul\nD Paris Support B"
        l = diplomacy_read(s)
        self.assertEqual(l, ["A Istanbul Hold", "B Moscow Move Istanbul", "C London Move Istanbul", "D Paris Support B"])
    def test_diplomacy_read3(self):
        s = "A Istanbul Hold\nB Moscow Move Istanbul\nC London Move Istanbul\nD Paris Support B\nE Austin Support A"
        l = diplomacy_read(s)
        self.assertEqual(l, ["A Istanbul Hold", "B Moscow Move Istanbul", "C London Move Istanbul", "D Paris Support B", "E Austin Support A"])

    # tests for diplomacy_solve (contains generatePlayersLocations and diplomacy_solve)
    def test_diplomacy_solve0(self):
        s = ["A Russia Move Korea", "B Korea Hold", "C England Move Korea", "D France Hold", "E Italy Support B"]
        l = diplomacy_solve(s)
        self.assertEqual(l, "A [dead]\nB Korea\nC [dead]\nD France\nE Italy\n")
    def test_diplomacy_solve1(self):
        s = ["A One Hold", "B Two Move One", "C Three Support B", "D Four Move Three"]
        l = diplomacy_solve(s)
        self.assertEqual(l, "A [dead]\nB [dead]\nC [dead]\nD [dead]\n")
    def test_diplomacy_solve2(self):
        s = ["A Poland Hold", "B Germany Move Poland", "C Japan Support B", "D US Support A", "E Russia Support A", "F Italy Support B", "G England Support A"]
        l = diplomacy_solve(s)
        self.assertEqual(l, "A Poland\nB [dead]\nC Japan\nD US\nE Russia\nF Italy\nG England\n")
    def test_diplomacy_solve3(self):
        s = ["A NewBalance Support B", "B Converse Support C", "C Adidas Move Nike", "D Nike Move NewBalance"]
        l = diplomacy_solve(s)
        self.assertEqual(l, "A [dead]\nB Converse\nC Nike\nD [dead]\n")
    def test_diplomacy_solve4(self):
        s = ["A Madrid Hold"]
        l = diplomacy_solve(s)
        msg = "values are not unequal :/"
        self.assertNotEqual(l, "A Madrid\nB Barcelona\n", msg)
    def test_diplomacy_solve5(self):
        s = ["A Madrid Hold", "B Barcelona Hold"]
        l = diplomacy_solve(s)
        msg = "values are not unequal :/"
        self.assertNotEqual(l, "A [dead]\nB Barcelona\n")
    def test_diplomacy_solve6(self):
        s = ["A Madrid Hold", "B Barcelona Hold"]
        l = diplomacy_solve(s)
        self.assertEqual(l, "A Madrid\nB Barcelona\n")

# -----------
# main
# -----------
if __name__ == "__main__": # pragma: no cover
    main()