# -------
# imports
# -------

from io import StringIO
from os import W_OK
from tokenize import String
from unittest import main, TestCase

from Diplomacy import diplomacy_read, diplomacy_eval, diplomacy_print, diplomacy_solve

# -----------
# TestDiplomacy
# -----------


class TestDiplomacy(TestCase):
    # ----
    # read
    # ----

    # # -----
    # # print
    # # -----

    def test_print_1(self):
        w = StringIO()
        diplomacy_print(w, "A", "[dead]")
        self.assertEqual(w.getvalue(), "A [dead]\n")

    def test_print_2(self):
        w = StringIO()
        diplomacy_print(w, "B", "Barcelona")
        self.assertEqual(w.getvalue(), "B Barcelona\n")

    # -----
    # solve
    # -----
    def test_solve_1(self):
        r = StringIO("A Madrid Move Barcelona\nB Barcelona Move Madrid\n")

        w = StringIO()
        diplomacy_solve(r,w)

        self.assertEqual(w.getvalue(), "A Barcelona\nB Madrid\n")
    def test_solve_2(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\n")


        w = StringIO()
        diplomacy_solve(r,w)

        self.assertEqual(w.getvalue(), "A [dead]\nB Madrid\nC [dead]\nD Paris\n")
    
    def test_solve_3(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\n")

        w = StringIO()
        diplomacy_solve(r,w)
        
        self.assertEqual(w.getvalue(), "A [dead]\nB Madrid\nC London\n")

    def test_solve_4(self):
        r = StringIO("A Madrid Hold\nB Rome Hold\nC Milan Move Madrid\nD Naples Support A\n")

        w = StringIO()
        diplomacy_solve(r,w)

        self.assertEqual(w.getvalue(), "A Madrid\nB Rome\nC [dead]\nD Naples\n")
    
    def test_solve_5(self):
        r = StringIO("A Madrid Support B\nB Barcelone Move Rome\nC Rome Hold\n")

        w = StringIO()
        diplomacy_solve(r,w)

        self.assertEqual(w.getvalue(), "A Madrid\nB Rome\nC [dead]\n")

    def test_solve_6(self):
        r = StringIO("A Madrid Support B\nB Barcelona Move Rome\nC Rome Hold\nD Naples Move Madrid\n")

        w = StringIO()
        diplomacy_solve(r,w)

        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD [dead]\n")   

    
    
# ----
# main
# ----

if __name__ == "__main__":
    main()
'''#pragma: no cover'''
