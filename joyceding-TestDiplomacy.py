
# -------
# imports
# -------

from Diplomacy import diplomacy_solve, read_input, diplomacy_print, update
from unittest import main, TestCase
from io import StringIO



# -----------
# TestDiplomacy
# -----------


class TestDiplomacy (TestCase):

#3 corner cases:
#equal numbver of support 
#switch location still alive 
#if support no longer 


    def test_solve1(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support A\nD Paris Support B")
        w = StringIO()
        diplomacy_solve(r,w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC London\nD Paris")

    def test_solve2(self):
        r = StringIO("A Madrid Move Barcelona\nB Barcelona Move Madrid")
        w = StringIO()
        diplomacy_solve(r,w)
        self.assertEqual(w.getvalue(), "A Barcelona\nB Madrid")

    def test_solve3(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support A\nD Paris Support A\nE Edmunton Move London")
        w = StringIO()
        diplomacy_solve(r,w)
        self.assertEqual(w.getvalue(), "A Madrid\nB [dead]\nC [dead]\nD Paris\nE [dead]")

# ----
# main
# ----



if __name__ == "__main__": # pragma: no cover
    main()

